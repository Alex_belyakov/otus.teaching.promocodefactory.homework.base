﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }

        public Task<IEnumerable<T>> GetAllAsync() => Task.FromResult(Data);

        public Task<T> GetByIdAsync(Guid id) => Task.FromResult(Data.FirstOrDefault(x => x.Id == id));

        public Task DeleteByIdAsync(Guid id) => Task.Run(() => Data = Data.Where(d => d.Id != id));

        public async Task UpdateAsync(T element) => await DeleteByIdAsync(element.Id).ContinueWith(_ => AddAsync(element));
        
        public Task AddAsync(T element) => Task.Run(() => Data = Data.Append(element));
    }
}