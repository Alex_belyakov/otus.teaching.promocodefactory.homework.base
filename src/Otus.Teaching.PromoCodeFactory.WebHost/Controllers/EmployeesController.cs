﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> roleRepository)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x =>
                new EmployeeShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FullName = x.FullName,
                }).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        [HttpDelete("{id:guid}")]
        public async Task DeleteEmployee(Guid id)
        {
            await _employeeRepository.DeleteByIdAsync(id);
        }

        [HttpPost]
        public async Task CreateEmployee(EmployeeRequest employee)
        {
            var employeeModel = await BuildEmployeeAsync(employee);
            await _employeeRepository.AddAsync(employeeModel);
        }

        [HttpPut("{id:guid}")]
        public async Task UpdateEmployee(Guid id, EmployeeRequest employee)
        {
            var employeeModel = await BuildEmployeeAsync(id, employee);
            await _employeeRepository.UpdateAsync(employeeModel);
        }

        private async Task<List<Role>> GetRoles(IEnumerable<RoleRequest> roles)
        {
            var roleModels = new List<Role>();

            foreach (var role in roles)
            {
                var roleModel = await _roleRepository.GetByIdAsync(role.Id);
                roleModels.Add(roleModel);
            }

            return roleModels;
        }

        private async Task<Employee> BuildEmployeeAsync(Guid id, EmployeeRequest employee)
        {
            var roles = await GetRoles(employee.Roles);

            if (roles.Any(r => r == null))
            {
                BadRequest("Role not found");
            }

            return new Employee
            {
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                Email = employee.Email,
                AppliedPromocodesCount = employee.AppliedPromocodesCount,
                Id = id,
                Roles = roles
            };
        }

        private async Task<Employee> BuildEmployeeAsync(EmployeeRequest employee)
        {
            var roles = await GetRoles(employee.Roles);

            if (roles.Any(r => r == null))
            {
                return null;
            }

            return new Employee
            {
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                Email = employee.Email,
                AppliedPromocodesCount = employee.AppliedPromocodesCount,
                Id = Guid.NewGuid(),
                Roles = roles
            };
        }
    }
}